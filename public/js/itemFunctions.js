const itemsArray = []
const table = document.querySelector('#table')

const getItemArray = () => {
    return itemArray
}

const getToken = () => {
    token = localStorage.getItem('myPointsTable')
    if(!token){
	alert("redirecting to login page")
	location.assign("/")
     }
     return token
}

const addItems = async (name,price,discount) => {
   try {
	   const res = await fetch('/item', {
	     method: "POST", headers: {
		'Authorization' : token,
		'Content-Type': 'application/json',	    
	     },body: JSON.stringify({name,price,discount})
	   })
	   const data = await res.json()
	   return data
   } catch(e) {
	   alert("Failed to add data")
   }
}

const fetchItems = async () => {
    token = localStorage.getItem('myPointsTable')
    if(!token){
	alert("redirecting to login page")
	location.assign("/")
     }
    try{
	const res = await fetch('/items',{
	    method: 'GET', headers: {
	      'Authorization': token 
	    }
	})
     if(res.status === 200){
	 const items = await res.json()
	 items.forEach(item => {
	    itemsArray.push(item)
	 })
	 render()
     }
     else {
	 alert("redirecting to login page")
	 location.assign("/")
     }	
    }catch(e) {
	    alert("Error: Unable to fetch data")
    }
}

const updateItem = async(item,cashBackcell) => {
     if(!token){
	alert("redirecting to login page")
	location.assign("/")
     }
    try {
	   const res = await fetch('/items/'+item.id, {
	     method: "PATCH", headers: {
		'Authorization' : token,
		'Content-Type': 'application/json',	    
	     },body: JSON.stringify({itemname: item.itemname,itemprice: item.itemprice,discount: item.discount.replace('%','')})
	   })
	   if(res.status === 200){
	       alert("Updated")
	       cashBackcell.textContent = Math.floor(item.itemprice * (item.discount.replace('%','') / 100))
	       return res
	   }
	    else{
		alert("Unable to update")
		return res
	    }
   } catch(e) {
	   alert("Failed to update data catcj")
   }
}

const deleteItem = async (item) => {
   if(!token){
	alert("redirecting to login page")
	location.assign("/")
     }
      try {
	   const res = await fetch('/items/'+item.id, {
	     method: "DELETE", headers: {
		'Authorization' : token	    
	     }
	   })
	   if(res.status === 200){
	       const index = itemsArray.indexOf(item)
	       itemsArray.splice(index,1)
	       render() 
	       alert("Deleted")
	   }
	    else
		throw new Error()
   } catch(e) {
	   alert("Failed to delete data")
   }
}

const render = () => {
     if(itemsArray.length === 0)
	return
     table.textContent = ""
     const tableHead = ['','Name','Price','Discount','Cashback']
     const headRow = document.createElement('tr')	
     tableHead.forEach(t => {
	 const th = document.createElement('th')
	 th.textContent = t
	 th.style.border = t === '' ? '0px' : "1px solid black"
	 headRow.appendChild(th)
     })
     table.appendChild(headRow)

     itemsArray.forEach(item => {
	 const row = document.createElement('tr')
	 let modifiedItem = [] 
	 const cashBackcell = document.createElement('td')
	 cashBackcell.textContent = Math.floor(item.itemprice * (item.discount / 100))
	 const deleteButton = document.createElement('button')
	 //create edit button
	 const editButton = document.createElement('button')
	 //configure edit button    
	 editButton.textContent = 'Edit'
	 editButton.addEventListener('click', async () => {
	      editButton.textContent = editButton.textContent === 'Edit' ? 'Done' : 'Edit'
	      row.contentEditable = !row.isContentEditable
	      if(modifiedItem.length != 0){
		 const test = modifiedItem.pop()
		 const tempItem = {...item, itemname: test.itemname,itemprice: test.itemprice,discount: test.discount}
		 const res = await updateItem(tempItem,cashBackcell,modifiedItem)
		 if(res.status === 400) {
		     row.cells[2].textContent = item.discount+"%"
		     row.cells[1].textContent = item.itemprice
		     modifiedItem = []
	             return
		 }
		 row.cells[2].textContent = tempItem.discount+"%"
		 modifiedItem = []     
     	      }
	 })
	 //setup delete button
	 deleteButton.textContent = "X"
	 deleteButton.addEventListener('click', () => {
	    deleteItem(item) 	
	 })
	 row.appendChild(deleteButton)
	 const keys = ['itemname','itemprice','discount']

	 keys.forEach(key => {
             const cell = document.createElement('td')
	    
             cell.textContent = key === 'discount' ? item[key] + "%" : item[key]
	     
	     row.addEventListener('input', (e) => {
		 const cells = e.target.cells
		 modifiedItem.push({itemname: cells[0].textContent,itemprice: cells[1].textContent,
				 discount: cells[2].textContent.replace('%','')})
	     })
	     row.appendChild(cell)
	 })
	 cashBackcell.contentEditable = 'false'
	 
	 row.appendChild(cashBackcell)
	 row.appendChild(editButton)
	 table.appendChild(row)
     })
}

const mainDIV = document.querySelector('#profile')

const handleLogin = async () => {
    const token = localStorage.getItem("myPointsTable")
    console.log(token) 
    if(!token)
	location.assign("/login.html")
    try{
       const res = await fetch('/users/me', {
	    method: 'GET',headers: {
	       'Authorization' : token
	    }
      })
     if(res.status === 200){
	const user = await res.json()
	const h1 = document.createElement('h1')
	h1.textContent = user.user.name
	mainDIV.appendChild(h1)
     }
    } catch(e) {
	 alert("Unable to fetch user profile. Redirecting")
    }
}

handleLogin()

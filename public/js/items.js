
let token = ""
const divForm = document.querySelector('#divForm')
divForm.style.display = 'none'
const form = document.querySelector('#addForm')

const toggleForm = () => {
    if(divForm.style.display === 'flex')
	divForm.style.display = 'none'
    else
	divForm.style.display = 'flex'
}

document.querySelector('#add').addEventListener('click', (e) => {
     e.preventDefault()
     console.log(e.target.id)	
     toggleForm()
})

form.elements.submit.addEventListener('click',async (e) => {
   e.preventDefault()
   const itemname = form.elements.nameCell.value
   const itemprice = form.elements.priceCell.value
   const discount = form.elements.disCell.value
   
   const data = await addItems(itemname,itemprice,discount)
   if(data.name !== 'error'){
       itemsArray.push(data)
       render()
   }
   else
	alert("Invalid data submitted")
})

fetchItems()
render()

const form = document.querySelector('#signup')

document.querySelector('#sendForm').addEventListener('click', async (e) => {
	e.preventDefault()
	const email = form.elements.email.value
	const password = form.elements.password.value
	try{
		const res = await fetch("/signup",{method: "POST",headers: {
			'Content-Type': 'application/json',
			//'Authorization': 'haha'
	     	},
		body: JSON.stringify({email,password})
		})
			if(res.status === 200){
			   const ob = await res.json()
			   localStorage.setItem("myPointsTable", ob.token)
			    location.assign("/me.html")
		}
		else
			alert("Error: username already exists and/or password is less than 6 characters")

	}catch(e) {
		console.log("Unable to signup check email and/ password, Password length must me >= 6")
	}
	
})

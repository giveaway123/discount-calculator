const form = document.querySelector('#signup')

const handleLogin = async(email,password) => {
	
		try{
	   const res = await fetch("/login",{method: "POST",headers: {
		'Content-Type': 'application/json',
		//'Authorization': 'haha'
	     },
	    body: JSON.stringify({email,password})
	})
		if(res.status === 200){
			const ob = await res.json()
			console.log(ob.token)
			localStorage.setItem("myPointsTable", ob.token)
			location.assign("/me.html")	
		}
		else
			alert("Login failed")
	} catch(e) {
		console.log("failed")
	}
}

document.querySelector('#sendForm').addEventListener('click', (e) => {
	e.preventDefault()
	const email = form.elements.email.value
	const password = form.elements.password.value
	handleLogin(email,password)
})

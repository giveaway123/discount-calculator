const points = [
	{
	    name: "Final Fantasy VII Remake",
	    price: 2499,
	    discount: 5
	},
	{
	    name: "iOS charger",
	    price: 698,
	    discount: 5
	},
	{
	     name: "Pauita",
	     price: 215,
	     discount: 5
	}
]

const mainDIV = document.querySelector('#app')
document.querySelector('#divForm').style.display = 'none'
document.querySelector('#divForm').hidden = true
document.querySelector('#add').addEventListener('click',(e) => {
     e.preventDefault()
      document.querySelector('#divForm').style.display = 'flex'
     
})

//addForm.setAttribute('hidden',true)

const addRow = () => {
	const row = document.createElement('tr')
	return row
}

const addCell = () => {
	const cell = document.createElement('td')
	return cell
}
const addStyles = (node) => {
	node.style.border = "1px solid black"
}

const render = () => {
	const table = document.createElement('table')
	const col = addRow()
	const name = document.createElement('th')
	const price = document.createElement('th')
	const discount = document.createElement('th')
	const del = document.createElement('th')
	const cashback = document.createElement('th')
	addStyles(name)
	addStyles(price)
	addStyles(discount)
	addStyles(cashback)

	name.textContent = "Name"
	price.textContent = "Price"
	discount.textContent = "Discount"
	del.textContent = ""
	cashback.textContent = "Cashback"

	col.appendChild(del)
	col.appendChild(name)
	col.appendChild(price)
	col.appendChild(discount)
	col.appendChild(cashback)
	table.appendChild(col)
	points.forEach(p => {
	    const row = addRow()
	    const nameCell = addCell()
	    const priceCell = addCell()
	    const disCell = addCell()
	    const cashbackCell = addCell()
	    const button = document.createElement('button')
	    button.textContent = "X" 
	    
	    nameCell.style.border = "1px solid black"
	    addStyles(priceCell)
	    addStyles(disCell)
	    addStyles(cashbackCell)
	    nameCell.textContent = p.name
	    priceCell.textContent = p.price
	    disCell.textContent = p.discount + "%"
	    cashbackCell.textContent = p.price * (p.discount / 100)
	    row.appendChild(button) 
	    row.appendChild(nameCell)
	    row.appendChild(priceCell)
	    row.appendChild(disCell)
	    row.appendChild(cashbackCell)
	    table.appendChild(row)
	})

	mainDIV.appendChild(table)

}

render()



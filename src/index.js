const path = require('path')
const express = require('express')
const userRouter = require('../routers/userRouter')
const itemRouter = require('../routers/itemsRouter')
const db = require('../db/index')
const app = express()

const publicDir = path.join(__dirname,'../public')

app.use(express.static(publicDir))

const createSqlTable = async() => {
    const pgcrypto = "CREATE EXTENSION IF NOT EXISTS pgcrypto"
    const userTable = "CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, name TEXT UNIQUE, password TEXT)"
    const itemsTable = "CREATE TABLE IF NOT EXISTS items(id SERIAL PRIMARY KEY, user_id INTEGER REFERENCES users(id), itemname TEXT,itemprice REAL, discount REAL, itemdate TIMESTAMPTZ)"
    const userToken =  "CREATE TABLE IF NOT EXISTS token(id SERIAL PRIMARY KEY, user_id INTEGER REFERENCES users(id), userToken TEXT)"	
    try{
	await db.query(pgcrypto,[])
    	await db.query(userTable,[]) 
	await db.query(itemsTable,[])
	await db.query(userToken,[])
	
    } catch(e) {
	console.log("error")
    }
}

app.get("/",(req,res) => {
	res.sendFile(publicDir +"/login.html")
})

app.get("/signup",(req,res) => {
	res.sendFile(publicDir + "/signup.html")
})

const PORT = process.env.PORT || 3000
app.use(express.json())

app.use(userRouter)
app.use(itemRouter)

app.listen(PORT, () => {
	console.log("works")
})

createSqlTable()

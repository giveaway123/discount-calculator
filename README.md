### Discount Calculator
Web app deployed on heroku [Discount Calculator](https://pointscalc.herokuapp.com/). It uses postgresSQL as database.

### /src/Index.js
This is where the web app starts. This is similar to main method in some programming languages. It setup port, binds userRouter and itemRouter, creates a new table if it does not exists.
It also has two HTTP get method for signup and login page.

### Signup
File: pointsCalc/routers/userRouter.js 

This is a HTTP post method that accepts a username and password. In the [Signup](https://pointscalc.herokuapp.com/signup) page, when a user click on submit button, signup post method is invoked. It inserts the user infomation on to the database,only if password
is at least 6 chars and is not "password". It also generates a unique token to, so that user do not need to login again for the specified time in jwt, using jsonwebtoken and returns unique token to client side. 

### Login
File: pointsCalc/routers/userRouter.js 
It is same signup API except it checks wheather the user and password are valid or not. If they are a new token is generated and returned to client side. When the user is logged int this token is used
to authenticate the user.

### Patch
File: pointsCalc/routers/userRouter.js 

It is a HTTP patch method which allows logged in users to change the username and password. It uses token generated in login or signup to authenticate users.

### Logout
File: pointsCalc/routers/userRouter.js 

HTTP POST method to logut users.

It logs out users by deleting their authentication token generated in signup or login process.
### ItemsRouter
Methods in pointsCalc/routers/itemRouter.js are similar to method in userRouter.js except that it adds,updates,removes items from the database and requires a valid token to use methods.

### DB Connection
FILE: pointsCalc/db/index.js

It is a connection to database url and uses environment variable specified in heroku postgres details.

### Authentication
FILE: pointsCalc/middleware/auth.js

This method verify whether the user token is valid or not. if not valid it asks to user to login.

const db = require('../db/index')
const jwt = require('jsonwebtoken')

const generateToken = (id) => {
	const token =  jwt.sign({_id: id},process.env.MY_KEY)
	return token
}

const saveToken = async(id,token) => {
	const text = "INSERT INTO token(user_id,userToken) VALUES($1,$2) RETURNING *"
	const values = [id,token]
	const {rows} = await db.query(text,values)
	return rows
}

const authenticate = async(name,pass) => {
	const verifyName = "SELECT * FROM users WHERE name = $1 AND password = crypt($2,password)"
	try {
		const {rows} =  await db.query(verifyName,[name,pass])
		if(rows[0].length == 0)
			throw new Error("Invalid login details")
		return rows
	}  catch(e) {}
}

module.exports = {generateToken,
	saveToken,authenticate}

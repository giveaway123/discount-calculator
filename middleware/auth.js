const jwt = require('jsonwebtoken')
const db = require('../db/index')

const auth = async (req,res,next) => {
	const text = "SELECT id,name FROM users WHERE id = $1"
	const verifyTok = "SELECT * FROM token WHERE user_id = $1 AND userToken = $2"
	try{
		const token = req.header('Authorization').replace('Bearer ', '')
		const decodedToken = jwt.verify(token, process.env.MY_KEY)
		const isValidToken = await db.query(verifyTok,[decodedToken._id,token])
		if(isValidToken.rows[0].length == 0)
			throw new Error()
		const user = await db.query(text,[decodedToken._id])
		req.user = user.rows[0]
		req.token = token
		next()
	} catch(e) {
		res.status(401).send({error: 'Please login'})
	}
}

module.exports = auth

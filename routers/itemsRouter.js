const express = require('express')
const db = require('../db/index')
const auth = require('../middleware/auth')
const format = require('pg-format')
const router = new express.Router()

router.post("/item",auth,async(req,res) => {
	console.log(req.body)
	const date = new Date()
	console.log("date is ", date)
	if(!req.body.discount || req.body.discount < 0)
	    req.body.discount = 0
	const sql = "INSERT INTO items(user_id,itemname,itemprice,discount,itemdate)" +
	"VALUES($1,$2,$3,$4,current_timestamp) RETURNING *"
	const val = [req.user.id, req.body.name, req.body.price, req.body.discount]

	try {
		const {rows} = await db.query(sql,val)
		console.log(rows)
		res.send(rows[0])
	} catch(e) {
		res.status(400).send(e)
	}
})

router.patch("/items/:id",auth,async(req,res) => {
    
    const allowedUpdates = ['itemname','itemprice','discount']
    const updates = Object.keys(req.body)
    const isValid = updates.every(update => allowedUpdates.includes(update))
    if(!isValid) {
		return res.status(400).send({Error: "Invalid updates!"})
	} 
	if(isNaN(req.body.itemprice) || isNaN(req.body.discount)) {
		return res.status(400).send()
	}
	if(req.body.discount < 0) {
		return res.status(400).send()
	}
     try {
	   updates.forEach(async (update) => {
	        const sql = format("UPDATE items SET %I = %L WHERE id = %s", update,req.body[update],req.params.id)
		console.log(sql)
		 await db.query(sql,[])
	   })
	   res.send()
     } catch(e) {
	     res.status(400).send()
     }
})

router.delete("/items/:id",auth,async(req,res) => {
    const id = req.params.id
    const sql = "DELETE FROM items WHERE id = $1 RETURNING *"
    try {
	  const {rows} = await db.query(sql,[id])
	  console.log(rows)
	  if(rows.length === 0)
	    return res.status(404).send({Error: "Item not found"})
	  res.send()
    } catch(e) {
	 res.status(400).send()
    }
})

router.get("/items",auth,async(req,res) => {
	const id = req.user.id
	const sql = "SELECT * FROM items WHERE user_id = $1 ORDER BY id"
	try{
		const {rows} = await db.query(sql,[id])
		res.send(rows)
	} catch(e) {
		res.status(400).send(e)
	}
})

module.exports = router

const express = require('express')
const db = require('../db/index')
const validator = require('validator')
const auth = require('../middleware/auth')
const userFunc = require('../userFunctions/userFunc')
const format = require('pg-format')

const router = new express.Router()

router.post("/signup",async (req,res) => {
	console.log(req.body)
	console.log(req.header('Authorization'))
	const text = "INSERT INTO USERS(name,password)VALUES($1,crypt($2,gen_salt('bf'))) RETURNING *"
	const values = [req.body.email,req.body.password]
	const pass = req.body.password
	try{
		if(!validator.isLength(pass,{min: 6}) || pass.toLowerCase().includes("password")) 
			throw new Error("Invalid password")
		const {rows} = await db.query(text,values)
		const token = userFunc.generateToken(rows[0].id)
		await userFunc.saveToken(rows[0].id,token)
		res.send({rows,token: token})

	} catch(e) {
		res.status(400).send({error: "err"})
	}
})

router.post("/login",async(req,res) => {
	try{	
		const rows = await userFunc.authenticate(req.body.email,req.body.password)
		const token =  userFunc.generateToken(rows[0].id)
		await userFunc.saveToken(rows[0].id,token)
		res.send({user: rows[0], token: token})
	} catch(e) {
		res.status(401).send(e)
	}
})

router.get("/users/me",auth, async(req,res) => {
	res.send({user: req.user, token: req.token})
})

router.patch("/update",auth,async(req,res) => {
	const name = req.body.email
	const pass = req.body.password

	if(!validator.isLength(pass,{min: 6}) || pass.toLowerCase().includes("password")) 
		return res.send("invalid arguments")

	const emailQ = "UPDATE users SET name = $1 WHERE id = $2"
	const passQ = "UPDATE users SET password = crypt($1,gen_salt('bf')) WHERE id = $2"
	const allowedUpdates = ['email','password']
	const updates = Object.keys(req.body)

	const isValidUpdates = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdates)
		return res.status(404).send()

	try {
		updates.forEach(async(update) => {
		   if(update === 'email')
			await db.query(emailQ,[name,req.user.id])
		   if(update === 'password')
			await db.query(passQ,[pass,req.user.id])
		})
		res.send({user: req.user, token: req.token})
	} catch(e) {
		res.status(401).send(e)
	}	
})

router.post("/logout",auth,async(req,res) => {
	const sql = "DELETE FROM token WHERE user_id = $1 AND userToken = $2"
	try{
		await db.query(sql,[req.user.id,req.token])
		res.send()
	} catch(e) {
		res.status(401).send(e)
	}
})

router.post("/logoutAll",auth,async(req,res) => {
	const sql = "DELETE FROM token WHERE user_id = $1"
	try {
		await db.query(sql,[req.user.id])
		res.send()
	} catch(e) {
		res.status(400).send(e)
	}
})



module.exports = router
